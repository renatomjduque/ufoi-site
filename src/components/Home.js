import { Hero, Button, InlineLink } from '@algolia/ui-library';
import { useColorMode } from '@docusaurus/theme-common';
import { useBaseUrlUtils } from '@docusaurus/useBaseUrl';
import React from 'react';

import SiteLogo from './SiteLogo';
import instanceMembers from './instance-members.json';

function Home() {
  const { withBaseUrl } = useBaseUrlUtils();
  const { colorMode } = useColorMode();

  React.useEffect(() => {
    if (colorMode === 'dark') {
      document.querySelector('html').classList.add('dark');
    } else {
      document.querySelector('html').classList.remove('dark');
    }
  }, [colorMode]);

  function Header() {
    return (
      <Hero
        id="hero"
        title={<SiteLogo width="100%" />}
        background="curves"
        cta={[
          <Button
            key="get-started"
            href={withBaseUrl('docs/getting-started')}
          >
            Get started
          </Button>,
          <Button
            key="apply"
            href={withBaseUrl('docs/apply-info')}
            background="blue"
            color="white"
            className="apply-button"
          >
            Apply
          </Button>,
        ]}
      />
    );
  }

  function Description() {
    return (
      <>
        {/* Instance Members */}
        <div className="py-16 overflow-hidden">
          <div className="relative max-w-xl mx-auto px-4 md:px-6 lg:px-8 lg:max-w-screen-xl">
            <div className="max-w-screen-xl mx-auto px-4 md:px-6 lg:px-8">
              <div className="max-w-4xl mx-auto text-center">
                <h2 className="text-3xl leading-9 font-extrabold md:text-4xl md:leading-10">
                  A federation of good-faith actors on the Fediverse
                </h2>
              </div>
            </div>
            <div className="pt-4 pb-12 md:pb-16">
              <div className="relative">
                <div className="relative max-w-screen-xl mx-auto px-4 lg:px-6">
                  <div className="max-w-4xl mx-auto">
                    <dl className="rounded-lg shadow-xl lg:grid lg:grid-cols-3 instance-members">
                      <div className="flex flex-col border-b p-6 text-center lg:border-0 instance-members-border">
                        <dt
                          className="order-2 mt-2 text-lg leading-6 font-medium text-description"
                          id="item-1"
                        >
                          Fediverse Coverage
                        </dt>
                        <dd
                          className="order-1 text-5xl leading-none font-extrabold text-indigo-600"
                          aria-describedby="item-1"
                        >
                          +2.5%
                        </dd>
                      </div>
                      <div className="flex flex-col border-t border-b p-6 text-center lg:border-0 lg:border-l instance-members-border">
                        <dt className="order-2 mt-2 text-lg leading-6 font-medium text-description">
                          Instance members
                        </dt>
                        <dd className="order-1 text-5xl leading-none font-extrabold text-indigo-600">
                          16
                        </dd>
                      </div>
                      <div className="flex flex-col border-t p-6 text-center lg:border-0 lg:border-l instance-members-border">
                        <dt className="order-2 mt-2 text-lg leading-6 font-medium text-description">
                          User members
                        </dt>
                        <dd className="order-1 text-5xl leading-none font-extrabold text-indigo-600">
                          +200,000
                        </dd>
                      </div>
                    </dl>
                  </div>
                </div>
              </div>
            </div>
            <div className="mt-8 grid grid-cols-4 gap-0.5 md:grid-cols-6 lg:mt-0 lg:grid-cols-8">
              {instanceMembers.map(({ name, href, image }) => (
                <div
                  key={href}
                  className="col-span-1 flex justify-center py-2 px-2 text-center"
                >
                  <a
                    href={href}
                    rel="noreferrer"
                    target="_blank"
                    alt={`Discover UFoI on the ${name} instance`}
                  >
                    <img
                      className="inline-block h-10 w-10"
                      src={withBaseUrl(image)}
                      alt={`Discover UFoI on the ${name} instance`}
                    />
                    <div className="text-description uppercase text-xs py-2 font-semibold">
                      {name}
                    </div>
                  </a>
                </div>
              ))}
            </div>
          </div>
        </div>

        {/* Description */}
        <div className="py-16 overflow-hidden">
          <div className="relative max-w-xl mx-auto px-4 md:px-6 lg:px-8 lg:max-w-screen-xl">
            <div className="relative">
              <h3 className="text-center text-3xl leading-8 font-extrabold tracking-tight md:text-4xl md:leading-10">
                No more mobs with pitchforks!
              </h3>
              <p className="mt-4 max-w-3xl mx-auto text-center text-xl leading-7 text-description">
                 A united federation based on ethics, evidence, and due process.
              </p>
            </div>

            <div className="pt-16">
              <ul className="lg:grid lg:grid-cols-3 lg:col-gap-8 lg:row-gap-10">
                <li>
                  <div className="flex">
                    <div className="flex-shrink-0">
                      <div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
                        <svg
                          viewBox="0 0 20 20"
                          fill="currentColor"
                          className="search w-6 h-6"
                        >
                          <path
                            fillRule="evenodd"
                            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                            clipRule="evenodd"
                          ></path>
                        </svg>
                      </div>
                    </div>
                    <div className="ml-4">
                      <h4 className="text-lg leading-6 font-medium">
                        Guaranteed federation
                      </h4>
                      <p className="mt-2 text-base leading-6 text-description">
                        As long as members of the UFoI uphold the UFoI's 
                        {' '}<InlineLink
                          href={withBaseUrl("/docs/code-of-ethics")}
                         >
                          Code of Ethics
                        </InlineLink>{' '}
                        you are guaranteed to be federated with the
                        whole of the UFoI.
                      </p>
                    </div>
                  </div>
                </li>
                <li className="mt-10 lg:mt-0">
                  <div className="flex">
                    <div className="flex-shrink-0">
                      <div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
                        <svg
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                          className="user-group w-6 h-6"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"
                          ></path>
                        </svg>
                      </div>
                    </div>
                    <div className="ml-4">
                      <h4 className="text-lg leading-6 font-medium">
                        Due process
                      </h4>
                      <p className="mt-2 text-base leading-6 text-description">
                        We guarantee all members of the UFoI to be given a fair
                        hearing with due process, strictly evidence based (no 
                        hearsay) and a chance for both sides to be heard,
                      </p>
                    </div>
                  </div>
                </li>
                <li className="mt-10 lg:mt-0">
                  <div className="flex">
                    <div className="flex-shrink-0">
                      <div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
                        <svg
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                          className="device-mobile w-6 h-6"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M12 18h.01M8 21h8a2 2 0 002-2V5a2 2 0 00-2-2H8a2 2 0 00-2 2v14a2 2 0 002 2z"
                          ></path>
                        </svg>
                      </div>
                    </div>
                    <div className="ml-4">
                      <h4 className="text-lg leading-6 font-medium">
                        Right to Leave
                      </h4>
                      <p className="mt-2 text-base leading-6 text-description">
                        No obligations for instance admins. Instances are allowed to
                        leave the UFoI at anytime with no prior notice. If you arent sure
                        about membership you are never locked in.
                      </p>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>

        {/* How it works */}
        <div className="diagonal-box py-16 bg-gray-200 overflow-hidden">
          <div className="diagonal-content max-w-xl mx-auto px-4 md:px-6 lg:px-8 lg:max-w-screen-xl">
            <div className="max-w-screen-xl mx-auto pt-6 px-4 md:px-6 lg:px-8">
              <div className="max-w-4xl mx-auto text-center">
                <h2 className="text-3xl leading-9 font-extrabold text-gray-900 md:text-4xl md:leading-10">
                  How it works
                </h2>
                <p className="mt-4 max-w-2xl text-xl leading-7 text-gray-500 lg:mx-auto">
                  The UFoI is a social construct, no special software needed. Any Fediverse 
                  instance may join so long as you agree to follow a few basic rules.
                </p>
              </div>
            </div>

            <div className="py-16">
              <div className="max-w-xl mx-auto px-4 md:px-6 lg:max-w-screen-lg lg:px-8 ">
                <div className="lg:grid lg:grid-cols-3 lg:gap-8">
                  <div>
                    <div className="flex items-center justify-center">
                      <img
                        className="h-200"
                        src={withBaseUrl('img/assets/scraping.svg')}
                        width="190px"
                        height="220px"
                        alt="Waiting list"
                      />
                    </div>
                    <div className="mt-10 lg:mt-0 p-4">
                      <h5 className="text-lg leading-6 font-medium text-gray-900">
                        1. Code of Ethics
                      </h5>
                      <p className="mt-2 text-base leading-6 text-gray-600">
                        We require all member instances to adhere to a simple
                        {' '}<InlineLink
                          href={withBaseUrl("/docs/code-of-ethics")}
                         >
                          Code of Ethics
                        </InlineLink>{' '}
                        that must be applied to their moderation practices. Don't worry, 
                        you are welcome to add your own instance rules on top of that.
                      </p>
                    </div>
                  </div>
                  <div className="mt-10 lg:mt-0 p-4">
                    <div className="h-200 flex items-center justify-center">
                      <img
                        src={withBaseUrl('img/assets/configuration.svg')}
                        width="140px"
                        height="220px"
                        alt="waiting processing"
                      />
                    </div>
                    <div>
                      <h5 className="text-lg leading-6 font-medium text-gray-900">
                        2. Apply for Membership
                      </h5>
                      <p className="mt-2 text-base leading-6 text-gray-600">
                         Any instance, big or small, can join. Once you apply
                         your instance will be reviewed to certify that you do
                         indeed follow the
                         {' '}<InlineLink
                          href={withBaseUrl("/docs/code-of-ethics")}
                         >
                          Code of Ethics
                        </InlineLink>{' '}.
                      </p>
                    </div>
                  </div>
                  <div className="mt-10 lg:mt-0 p-4">
                    <div className="h-200 flex items-center justify-center">
                      <img
                        src={withBaseUrl('img/assets/implementation.svg')}
                        width="220px"
                        height="220px"
                        alt="all done"
                      />
                    </div>
                    <div>
                      <h5 className="text-lg leading-6 font-medium text-gray-900">
                        3. Guaranteed Federation
                      </h5>
                      <p className="mt-2 text-base leading-6 text-gray-600">
                         Once admitted your instance will be guaranteed federation and
                         must likewise federate with all servers within the UFoI.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Anatomy of UFoI */}
        <div className="py-16 overflow-hidden lg:py-24">
          <div className="relative max-w-xl mx-auto px-4 md:px-6 lg:px-8 lg:max-w-screen-xl">
            <div className="relative mt-12 lg:mt-24 lg:grid lg:grid-cols-2 lg:gap-8 lg:items-center">
              <div className="relative">
                <h4 className="text-2xl leading-8 font-extrabold tracking-tight md:text-3xl md:leading-9">
                  Overview of UFoI
                </h4>
                <p className="mt-3 text-lg leading-7 text-description">
                  The Fediverse doesnt need to be a hostile place -- when people work together to nurture the best
                  in each other then we can build a community to be proud of.
                </p>

                <ul className="mt-10">
                  <li>
                    <div className="flex">
                      <div className="flex-shrink-0">
                        <div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
                          <svg
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            className="sparkles w-6 h-6"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              d="M5 3v4M3 5h4M6 17v4m-2-2h4m5-16l2.286 6.857L21 12l-5.714 2.143L13 21l-2.286-6.857L5 12l5.714-2.143L13 3z"
                            ></path>
                          </svg>
                        </div>
                      </div>
                      <div className="ml-4">
                        <h5 className="text-lg leading-6 font-medium">
                          Focus on the community
                        </h5>
                        <p className="mt-2 text-base leading-6 text-description">
                          The UFoI is focused on building communities, not tearing them down. Our focus is to create a community 
                          that is proud to federate with each other. This bears a striking difference to the way we usually
                          approach online communities where the primary tool is blocking.
                        </p>
                      </div>
                    </div>
                  </li>
                  <li className="mt-10">
                    <div className="flex">
                      <div className="flex-shrink-0">
                        <div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
                          <svg
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            className="menu-alt2 w-6 h-6"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              d="M4 6h16M4 12h16M4 18h7"
                            ></path>
                          </svg>
                        </div>
                      </div>
                      <div className="ml-4">
                        <h5 className="text-lg leading-6 font-medium">
                          Block lists are secondary
                        </h5>
                        <p className="mt-2 text-base leading-6 text-description">
                          There will always be a need to maintain block lists for some instances. Coalitions provide
                          an organized way to do this while still ensuring due process. Coalitions are community
                          formed so the core principles of UFoI can focus on creating connections.
                        </p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>

              <div className="mt-10 -mx-4 lg:mt-0 uil-ta-center">
                <img
                  className="relative mx-auto image-rendering-crisp"
                  src={withBaseUrl(
                    "img/fediverse-colors.png"
                  )}
                  alt="fediverse-colors"
                />
              </div>
            </div>

            <div className="relative mt-12 md:mt-16 lg:mt-24">
              <div className="lg:grid lg:grid-flow-row-dense lg:grid-cols-2 lg:gap-8 lg:items-center">
                <div className="lg:col-start-2">
                  <ul className="mt-10">
                    <li>
                      <div className="flex">
                        <div className="flex-shrink-0">
                          <div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
                            <svg
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="currentColor"
                              className="lightning-bolt w-6 h-6"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M13 10V3L4 14h7v7l9-11h-7z"
                              ></path>
                            </svg>
                          </div>
                        </div>
                        <div className="ml-4">
                          <h5 className="text-lg leading-6 font-medium">
                            Still decentralized
                          </h5>
                          <p className="mt-2 text-base leading-6 text-description">
                            The UFoI still strongly believes in keeping things decentralized. Not everyone in the Fediverse
                            will want to be part of the UFoI; we still allow instances to freely federate outside of the UFoI
                            and encourage others to create their own federations modeled after the UFoI, it is open-source
                            afterall.
                          </p>
                        </div>
                      </div>
                    </li>
                    <li className="mt-10">
                      <div className="flex">
                        <div className="flex-shrink-0">
                          <div className="flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
                            <svg
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="currentColor"
                              className="arrows-expand w-6 h-6"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M4 8V4m0 0h4M4 4l5 5m11-1V4m0 0h-4m4 0l-5 5M4 16v4m0 0h4m-4 0l5-5m11 5l-5-5m5 5v-4m0 4h-4"
                              ></path>
                            </svg>
                          </div>
                        </div>
                        <div className="ml-4">
                          <h5 className="text-lg leading-6 font-medium">
                            Transparent governance
                          </h5>
                          <p className="mt-2 text-base leading-6 text-description">
                            All decisions are made publicly for transparency, including all evidence and voting records. Decisions
                            are made democratically and in the open ensuring all parties effected have a chance to present their side.
                          </p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>

                <div className="mt-10 -mx-4 lg:mt-0 lg:col-start-1 uil-ta-center">
                  <img
                    className="relative mx-auto"
                    width="490"
                    src={withBaseUrl('img/fediverse.png')}
                    alt="anatomy-of-ufoi"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

      </>
    );
  }

  return (
    <div id="tailwind">
      <Header />
      <Description />
    </div>
  );
}

export default Home;
