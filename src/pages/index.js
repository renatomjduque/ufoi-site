import Layout from '@theme/Layout';
import React from 'react';

import Home from '../components/Home';

function HomePage() {
  return (
    <Layout
      title="United Federation of Instances"
      description="A federation of good-faith actors on the Fediverse"
    >
      <Home />
    </Layout>
  );
}

export default HomePage;
