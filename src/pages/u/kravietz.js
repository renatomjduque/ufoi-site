import { Hero } from '@algolia/ui-library';
import Layout from '@theme/Layout';
import React from 'react';

import UserMember from '../../components/UserMember.js';
import SiteLogo from '../../components/SiteLogo';

function UserKravietzPage() {
  return (
    <Layout
      title="UFoI: User Member Kravietz"
      description="A federation of good-faith actors on the Fediverse"
    >
      <div className="uil-pb-24">
        <Hero
          id="user-member"
          title={<SiteLogo width="100%" />}
          background="curves"
        />
        <UserMember name="kravietz"/>
      </div>
    </Layout>
  );
}

export default UserKravietzPage;
