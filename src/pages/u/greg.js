import { Hero } from '@algolia/ui-library';
import Layout from '@theme/Layout';
import React from 'react';

import UserMember from '../../components/UserMember.js';
import SiteLogo from '../../components/SiteLogo';

function UserGregPage() {
  return (
    <Layout
      title="UFoI: User Member"
      description="A federation of good-faith actors on the Fediverse"
    >
      <div className="uil-pb-24">
        <Hero
          id="user-member"
          title={<SiteLogo width="100%" />}
          background="curves"
        />
        <UserMember name="greg"/>
      </div>
    </Layout>
  );
}

export default UserGregPage;
