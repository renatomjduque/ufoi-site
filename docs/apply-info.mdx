---
title: Applying
---

There are several different types of applications. The most important is for instance administrators, or their representatives, to have their instance join the United Federation of Instances. People who are not instance admins may also join under provisions discussed below.

## Getting my instance into the UFoI

In order to get your instance into the UFoI you must meet a few requirements:

- You must be the instance's administrator or a representative of the administrator.
- You must agree that when moderating members of your instance you will uphold our [Code of Ethics](code-of-ethics) as a minimum standard (you may add your own additional rules).
- You must agree that while you choose to remain in the UFoI (you can leave at any time), you will federate with all instances in the UFoI.
- Your instance can be any size from a single user instance to a large instance, but the instance must be online at the time of application.
- Once admitted, you must indicate on your Terms of Service page that you are a member of the UFoI and provide a clear link to the UFoI [Code of Ethics](code-of-ethics).

Acceptance into the UFoI is non-binding, meaning you can leave at any time with no prior notice. So if you are unsure, there's no risk to joining up! Feel free to join and you are welcome to change your mind. We only ask that if you decide to leave, please let us know so we can update our website.

If you agree to the above then there are several ways you can formally apply.

### Just ask

If you aren't technically savy enough to create a Merge Request as outlined below, thats okay! You can just reach out to any council member and request they add you manually.

First find a council member to reach out to on the fediverse, you can find a list of them at the following link:

[/council-members](/council-members)

:::info

At the moment the UFoI is new, so we havent launched our voting facilities yet. As such the only council member at the moment is the founder, but this will change in the near future.

:::

Send them a message following the message template below.

```
Hi, I am the administrator of my instance and I would like to have my instance join the #UFoI

instance name: <instance name, no symbols, will render as all uppercase>
instance URL: <url to instance>
fedihandle: <list of all fediverse handles you want to be visible>
pgp fingerprint: <optional, your pgp fingerprint, all uppercase, in groups of 4 characters>
emails: <list of any emails you want visible>

<attach a square image to act as your instance logo>
```

Once reviewed we should have your instance added in no time.

### Create a Merge Request

The preferred way to apply is to simply go to our [official GitLab repository](https://gitlab.com/ufoi/ufoi-site/) and create a Merge Request to add yourself. To do this you only need to modify two files and add your logo.

First, make sure you have a GitLab account and are logged in.

Next, to add your logo, simply [go here](https://gitlab.com/ufoi/ufoi-site/-/new/master/static%2Fimg%2Finstance-logos) and add it. Please make sure you follow the existing format for the logo file names, they should be all lowercase, use underscores, and reflect your instance's domain name.

Next [edit this file](https://gitlab.com/-/ide/project/ufoi/ufoi-site/edit/master/-/src/components/instance-members.json) and add an entry for your instance. Make sure the name is unique, contains only lowercase letters, numbers, and spaces, and that the image points to the logo you uploaded in the previous step. A typical entry looks like this:

```json
{
  "name": "my instance name",
  "image": "img/instance-logos/my_instance_com.png",
  "href": "https://bar.com"
}
```

In the above code, `name` is the unique identifier for your instance. It is also used in all uppercase form as the display name. The `image` should point to the logo you created above. The `href` should point to your instance's main page (not a specific users page).

Finally [add yourself here](https://gitlab.com/ufoi/ufoi-site/-/blob/master/src/components/user-members.json) with your personal information. If you are adding more than one instance you should add only a single entry here: the format supports multiple profiles.

```json
{
  "name": "foobar",
  "display_name": "Foo Bar",
  "profiles": [
    {
      "handle": "@foo@bar", 
      "url": "https://bar.com/@foo"
    },
    {
      "handle": "@thing@foobar.com", 
      "url": "https://foobar.com/@thing"
    }
  ],
  "pgp_fingerprint": "1234 5678 90AB CDEF 1234 5678 90AB CDEF 1234 5678",
  "emails": ["FooBar@thing.dev", "foo@bar.com"],
  "instance_member_reps": ["my instance name", "my other instance name"],
  "on_council": false,
  "on_ufoi_instance": true
}

```

In the above the `name` should be any unique value, all lowercase letters and numbers, no spaces or symbols. This won't be used anywhere other than to generate the url to your profile that will be automatically generated. The `display_name` will be used on your auto-generated profile and can be stylized as you wish: it also doesn't need to be unique. The `profiles` array is a list of every `handle` you use on the fediverse that you want to include (you must include any handles from instances that are on the UFoI but other handles are optional). Obviously the `url` part of the profiles should be a link to your personal profile at the handle given. The `pgp_fingerprint` field should be your PGP fingerprint in all uppercase and in groups of 4. We strongly encourage including this, as we intend to use it later for voting. If you do not have one, you are welcome to skip that line entierly for now. The `emails` field is a list of your email addresses, and you can leave off this line if you don't want your email to be public. `instance_member_reps` is an important line, it should be an array of all the instances you added to the `instance-members.json` file above, and you must use the same exact spelling and capitalization. `on_council` should always be false, this is only true when a member is elected to the UFoI Council. Finally `on_ufoi_instance` will always be true as this indicates that at least one of your handles listed is from an instance on the UFoI. Since you are applying to join this will always be true.

:::caution
  It is very important that the url you specify above in the profiles section have exactly the same capitalization as your fediverse profile link. If you have the wrong capitalization the links will still work if you follow them, however, the green verification mark will not display.
:::

Once the merge request with this information is approved your instance will show on the front page and you will have a profile page verifying you as the representative for your instance. 

## Register as an Individual

If you are an individual and want to register please follow the above procedure just leave off the instance information and just provide your personal information. You can contact a council member or edit via a Merge Request.

There are two types of registered indiiduals, those that are on a UFoI member instance, and those that are not.

If you are on an instance that is a member of the UFoI then you are automatically a member of the UFoI by extension. However you are still encouraged to apply as we register you so you have the ability to vote in the future when voting opens up. Whether you register or not, you **must** follow the UFoI Code of Ethics as this will be enforced by your instance administrator.

If you are not on an instance that is a member of the UFoI, you can still register! By registering as an individual, you are agreeing to uphold the [Code of Ethics](code-of-ethics) even if not required by your instance admin.

In either case, once you register you will get a personalized UFoI profile that you can use in your fediverse profile that will get a green verified check mark showing others you are a "certified good actor" as a member of the UFoI.

## Profiles

All registered users, including Instance Member Representatives, will have a profile generated for them. The link will be at `UFoI.org/u/<name>` where `name` is the name you choose when adding your info to the `user-members.json` file. This profile contains information telling others who view it what your fediverse handle is and what your current status in the UFoI is.

The profiles use `rel=me` links. This means most fediverse instances will let you add the link to your profile and by doing so you will get a green check mark next to it automatically that tells others it is a verified link that is actually owned by you. This is the primary way we can identify members at a glance so it is highly encouraged you add this to your bio.
