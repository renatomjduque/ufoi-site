---
title: Bylaws
---

The bylaws of the UFI server as the mechanism by which it operates. It is distinctly seperate from the [Code of Ethics](code-of-ethics) since the bylaws describe the technical rather than the ethical side. A very high level summary of the bylaws are as follows:

- All instances members of the UFI agree to enforce the [Code of Ethics](code-of-ethics) in its moderation practices.
- All instance members of the UFI agree to federate with all instance members of the UFI, though they are free to defederate as they choose from instances outside of the UFI.
- Any instance members of the UFI found to violate the [Code of Ethics](code-of-ethics) shall be given a due process hearing and if found guilty will be expelled from the UFI.

:::info

The full bylaws can be found here: [https://ufoi.gitlab.io/constitution/united_federation_of_instances_bylaws.pdf](https://ufoi.gitlab.io/constitution/united_federation_of_instances_bylaws.pdf).

:::

:::caution

It is strongly encouraged that instances that are not members of the UFoI are not assumed to be bad-actors simply due to not being a member.

Any instance not within the UFoI should be judged on its own merit without prejudice.

:::
